import java.io.*;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {

	public static void main(String[] args) throws IOException {

		Map<String, Integer> totalResults = new HashMap<>();

		File docPath = new File("docs");

		deleteExistingFile();
		// create obejct of PrintWriter for finalReport file with the new date stamp and add the new file into the docs directory
		PrintWriter pw = new PrintWriter("docs/finalReport-" + getCurrentTimeStamp() + ".txt");

		// Get list of all the files in form of String Array
		String[] fileNames = docPath.list();
		// loop for reading the contents of all the files
		// in the directory docs
		for (String fileName : fileNames) {
			// create instance of file from Name of
			// the file stored in string Array
			File f = new File(docPath, fileName);

			// create object of BufferedReader
			BufferedReader br = new BufferedReader(new FileReader(f));

			// Read from current file
			String line = br.readLine();
			while (line != null) {

				// write to the finalReport file
				pw.println(line);
				line = br.readLine();
			}
			pw.flush();


			CommentAnalyzer commentAnalyzer=new CommentAnalyzer();
			commentAnalyzer.start();
		}

		//Reading contents from the final file
		File[] commentFiles = docPath.listFiles((d, n) -> n.contains("finalReport-" + getCurrentTimeStamp() + ".txt"));

		for (File commentFile : commentFiles) {

			CommentAnalyzer commentAnalyzer = new CommentAnalyzer(commentFile);

			//Create a list of Report keys you want to have,This can be made dynamic through user input
			List<CommentAnalyzer.ReportKeys> keys= Arrays.asList(new CommentAnalyzer.ReportKeys("Mover","MOVER_MENTIONS"),
					new CommentAnalyzer.ReportKeys("Shaker","SHAKER_MENTIONS"),
					new CommentAnalyzer.ReportKeys("?","QUESTIONS"));

			Map<String, Integer> fileResults = commentAnalyzer.analyze(keys);
			addReportResults(fileResults, totalResults);

		}

		System.out.println("RESULTS\n=======");
		totalResults.forEach((k, v) -> System.out.println(k + " : " + v));
	}

	/**
	 * This method adds the result counts from a source map to the target map
	 *
	 * @param source the source map
	 * @param target the target map
	 */
	private static void addReportResults(Map<String, Integer> source, Map<String, Integer> target) {

		for (Map.Entry<String, Integer> entry : source.entrySet()) {
			target.put(entry.getKey(), entry.getValue());
		}

	}

	/**
	 * This method generates current date stamp to append to the finalReport
	 *
	 * @return
	 */
	public static String getCurrentTimeStamp() {
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
		Date now = new Date();
		String strDate = sdfDate.format(now);
		return strDate;
	}
	/**
	 * This deletes existing file names to prevent duplicates
	 *
	 * @return
	 */
	public static void  deleteExistingFile() {
		try {

			Files.deleteIfExists(Paths.get("docs/finalReport-" + getCurrentTimeStamp() + ".txt"));
		} catch (NoSuchFileException e) {
			System.out.println("No such file/directory exists");
		} catch (DirectoryNotEmptyException e) {
			System.out.println("Directory is not empty.");
		} catch (IOException e) {
			System.out.println("Invalid permissions.");
		}

	}


}
