import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommentAnalyzer extends Thread {

	private File file;
	
	public CommentAnalyzer(File file) {
		this.file = file;
	}

	public CommentAnalyzer() {
	}

	public void run()
	{
		try
		{
			// Displaying the thread that is running
			System.out.println ("Thread " +
					Thread.currentThread().getId() +
					" is running");

		}
		catch (Exception e)
		{
			// Throwing an exception
			System.out.println ("Exception is caught");
		}
	}

	public Map<String, Integer> analyze(List<ReportKeys> reportKeysList) {

		Map<String, Integer> resultsMap = new HashMap<>();

		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			
			String line = null;
			while ((line = reader.readLine()) != null) {

				checksForKeyOccurences(resultsMap, line,reportKeysList);
			}
			
		} catch (FileNotFoundException e) {
			System.out.println("File not found: " + file.getAbsolutePath());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IO Error processing file: " + file.getAbsolutePath());
			e.printStackTrace();
		}
		
		return resultsMap;
		
	}

	/**
	 * This method will count occurrences for each key on the list
	 * @param resultsMap
	 * @param line
	 * @param reportKeysList
	 */
	private void checksForKeyOccurences(Map<String, Integer> resultsMap, String line,List<ReportKeys> reportKeysList) {

		if (line.length() < 15) {

			incOccurrence(resultsMap, "SHORTER_THAN_15");

		}
		if(isSpam(line)){
			incOccurrence(resultsMap, "SPAM");
		}
		else {
			//Calling the createResults method to handle related keys
			reportKeysList.forEach(item -> {
				createResults(resultsMap, line,item.getSearchKey(),item.getKey());
			});


		}
	}

	/**
	 * This method will handle the count for all the related key searches
	 * @param resultsMap
	 * @param line
	 * @param wildcart
	 * @param key
	 */
	private void createResults(Map<String, Integer> resultsMap, String line,String wildcart,String key) {
		if (line.contains(wildcart)) {

			incOccurrence(resultsMap, key);

		}

	}

	/**
	 * This method increments a counter by 1 for a match type on the countMap. Uninitialized keys will be set to 1
	 * @param countMap the map that keeps track of counts
	 * @param key the key for the value to increment
	 */
	private void incOccurrence(Map<String, Integer> countMap, String key) {
		
		countMap.putIfAbsent(key, 0);

		countMap.put(key, countMap.get(key) + 1);
	}
	/**
	 * This will check if there's a url in a text using regex
	 * @param target
	 * @return
	 */
	public static boolean isSpam(String target){

		String http = "((http:\\/\\/|https:\\/\\/)?(www.)?(([a-zA-Z0-9-]){2,}\\.){1,4}([a-zA-Z]){2,6}(\\/([a-zA-Z-_\\/\\.0-9#:?=&;,]*)?)?)";
		Pattern pattern = Pattern.compile(http);
		Matcher matcher = pattern.matcher(target);
		return matcher.find();

	}

	/**
	 * I created this class to be used as object creation for the report
	 */
	public static class ReportKeys{
		String searchKey,key;

		public ReportKeys(String searchKey, String key) {
			this.searchKey = searchKey;
			this.key = key;
		}

		public String getSearchKey() {
			return searchKey;
		}

		public void setSearchKey(String searchKey) {
			this.searchKey = searchKey;
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}
	}


}
